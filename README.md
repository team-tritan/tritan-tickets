[![Maintenance](https://img.shields.io/badge/Maintained%3F-no-red.svg)]
This discord bot is depreciated, do as you wish but this bot is not meant for use in production as it is currently.
---
*[For help and support join my discord server, click on this.](https://tritanbot.xyz/support)*


## Discord.js Ticket Bot | With Reactions
- I've moved to Gitlab! You can check out updated projects [here](https://gitlab.com/team-tritan).

## Packages to install
Here are the packages to install. You can click directly on the package to know how to install it.

- [discord.js](https://www.npmjs.com/package/discord.js)
- [dateformat](https://www.npmjs.com/package/dateformat)
- [quick.db](https://www.npmjs.com/package/quick.db)
- [fs](https://www.npmjs.com/package/fs)
- [path](https://www.npmjs.com/package/path)

So, here is the command to enter in your console. *(if quick.db has already been installed previously on your machine)*
```
npm install discord.js dateformat quick.db fs path
```
